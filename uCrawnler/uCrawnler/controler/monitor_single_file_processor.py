#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'thiago'

import sys
import time
import argparse


from recursos import Recursos
import util


def salve_contagem(recurso, contagem_palavras, contagem_paginas):
    with open(recurso.nome_padrao_arquivo_contagem_palavra,"w") as f:
        f.write(str(contagem_palavras))
    with open(recurso.nome_padrao_arquivo_contagem_paginas,"w") as f:
        f.write(str(contagem_paginas))

def parar_web_crawler(args):
    return util.executar_comando(args.comando_stop)


def main(args):
    rec = Recursos(args.usuario)

    if args.stopwords:
        util.carregar_stop_words()

    if args.pontuacao:
        util.carregar_pontuacao()

    util.executar_operacoes_pos_processamento(rec, args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Gera arquivo unico de corpora, enquanto o twitter crawler está em exucação, e conta suas palavras")
    parser.add_argument('-usuario', "--usuario", help='Identificação do usuário')
    parser.add_argument('--pontuacao', help='Eliminar pontuação', action='store_true')
    parser.add_argument('--stopwords', help='Eliminar stopwords', action='store_true')
    parser.add_argument('--limpar_caracteres', help='Limpar caracteres especiais', action='store_true')

    parser.add_argument('--ativar_entidades', help='Ativar entidades nomeadas', action='store_true')
    parser.add_argument('-formato_saida_entidades', "--formato_saida_entidades", help='Formato saída entidades nomeadas')

    parser.add_argument('--calcular_perplexidade', help="Calcular perplexidade", action='store_true')

    parser.add_argument('--ativar_ngram', help='Ativar cálculo de ngram', action='store_true')
    parser.add_argument('--gerar_vocabulario', help='Gerar vocabulário', action='store_true')
    parser.add_argument('-numero_gram', "--numero_gram", help='Valor do ngram', type=int)


    args = parser.parse_args(sys.argv[1:])
    main(args)
