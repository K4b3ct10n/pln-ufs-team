# -*- coding: utf-8 -*-


from recursos import Recursos
import commands


class NamedEntityController(object):

    def __init__(self, diretorio_padrao=Recursos.nome_diretorio_ner()):
        self.diretorio_padrao = diretorio_padrao

    # Invoca script que configura o diretario home do nome_usuario como sendo aquele passado como parametro
    # (diretorio_trabalho, nesse caso)
    def configuar_ner(self, arquivo, formato_saida):
        comando = "%s/script/runNER.sh '%s' '%s' '%s'" % \
                  (self.diretorio_padrao, self.diretorio_padrao, arquivo, formato_saida)
        return commands.getoutput(comando)
