#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'thiago'

import sys
import os
import time
import argparse


from recursos import Recursos
import codecs
import util


def salve_contagem(recurso, contagem_palavras, contagem_paginas):
    with open(recurso.nome_padrao_arquivo_contagem_palavra,"w") as f:
        f.write(str(contagem_palavras))
    with open(recurso.nome_padrao_arquivo_contagem_paginas,"w") as f:
        f.write(str(contagem_paginas))


def crawler_em_execucao(args):
    saida = util.executar_comando(args.comando_running)
    return saida[0] not in ("\n","")


def parar_web_crawler(args):
    return util.executar_comando(args.comando_stop)


def main(args):
    rec = Recursos(args.usuario)
    contagem_palavras = 0
    contagem_paginas = 0

    if args.stopwords:
        util.carregar_stop_words()

    if args.pontuacao:
        util.carregar_pontuacao()

    data_target = rec.nome_diretorio_data_target_usuario
    ok_dirs = {}
    hora_ultimo_arquivo_inserido = time.time()
    while crawler_em_execucao(args):
        with codecs.open(rec.nome_padrao_corpora, "a", "utf-8") as arq_saida:
            for diretorio in os.listdir(data_target):
                diretorio = os.path.join(rec.nome_diretorio_data_target_usuario, diretorio)
                if diretorio not in ok_dirs:
                    ok_dirs[diretorio] = os.path.join(diretorio, "ok")
                    os.makedirs(ok_dirs[diretorio])
                ok_dir = ok_dirs[diretorio]
                for nome_arquivo in os.listdir(diretorio):
                    if nome_arquivo.endswith("ok"):
                        continue
                    hora_ultimo_arquivo_inserido = time.time()
                    contagem_paginas += 1
                    nome_completo_arquivo = os.path.join(diretorio, nome_arquivo)
                    codificacao = util.get_encoding_type(nome_completo_arquivo)
                    with codecs.open(nome_completo_arquivo, "rU", codificacao) as arquivo:
                        try:
                            for linha in arquivo:
                                try:
                                    palavras, linha = util.processe_string(args, linha, "utf-8")
                                    contagem_palavras += palavras
                                    if args.qtd_palavras and contagem_palavras > args.qtd_palavras:
                                        util.executar_comando(args.comando_stop)
                                        util.executar_operacoes_pos_processamento(rec, args)
                                        sys.exit(0)
                                    arq_saida.write("%s\n" % linha)
                                except Exception as e:
                                    sys.stderr.write (str(e))
                        except Exception as e:
                            sys.stderr.write (str(e))
                    salve_contagem(rec, contagem_palavras, contagem_paginas)
                    try:
                        os.rename(nome_completo_arquivo, os.path.join(ok_dir, nome_arquivo))
                    except OSError:
                        os.remove(nome_completo_arquivo)
            if time.time() - hora_ultimo_arquivo_inserido > 60*10:
                util.executar_comando(args.comando_stop)
                break
            time.sleep(float(args.tempo_espera))
    util.executar_operacoes_pos_processamento(rec, args)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Gera arquivo unico de corpora, enquanto o web crawler está em exucação, e conta suas palavras")
    parser.add_argument('-usuario', "--usuario", help='Identificação do usuário')
    parser.add_argument('-wait',"--tempo_espera", help='Tempo de espera entre uma verificação e outra', type=float)
    parser.add_argument('-cmd_running',"--comando_running", help='Comando para verificar se o crawler está em execução')
    parser.add_argument('-cmd_stop',"--comando_stop", help='Comando para parar o crawler')
    parser.add_argument('--pontuacao', help='Eliminar pontuação', action='store_true')
    parser.add_argument('--stopwords', help='Eliminar stopwords', action='store_true')
    parser.add_argument('--limpar_caracteres', help='Limpar caracteres especiais', action='store_true')
    parser.add_argument('-qtd_palavras', help='Quantidade aproximada de palavras a se buscar', type=int)

    parser.add_argument('--ativar_entidades', help='Ativar entidades nomeadas', action='store_true')
    parser.add_argument('-formato_saida_entidades', "--formato_saida_entidades", help='Formato saída entidades nomeadas')
    parser.add_argument('-tipo_saida', "--tipo_saida", help='Tipo de saída do corpus')

    parser.add_argument('--ativar_ngram', help='Ativar cálculo de ngram', action='store_true')
    parser.add_argument('--gerar_vocabulario', help='Gerar vocabulário', action='store_true')
    parser.add_argument('--calcular_perplexidade', help="Calcular perplexidade", action='store_true')
    parser.add_argument('-numero_gram', "--numero_gram", help='Valor do ngram', type=int)

    args = parser.parse_args(sys.argv[1:])
    main(args)
