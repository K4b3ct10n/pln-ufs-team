# -*- coding: utf-8 -*-

from recursos import Recursos
from DadosProcessamento import DadosProcessamento


from single_file_processor import SingleFileProcessor

__author__ = 'thiago'

from twitter_crawler_control import TwitterCrawlerFacade
from news_crawler_control import NewsCrawlerFacade

class CrawlerFactory():

    def __init__(self, nome_usuario, recursos):
        self.nome_usuario = nome_usuario
        self.rec = recursos

    def processamento_em_execucao(self):
        nc = self.get_news_crawler_facade()
        tc = self.get_twitter_crawler_facade()
        sf = self.get_single_file_processor()

        return nc.processamento_em_execucao or \
               tc.processamento_em_execucao or \
               sf.processamento_em_execucao

    def get_crawler_em_execucao(self):
        nc = self.get_news_crawler_facade()
        tc = self.get_twitter_crawler_facade()
        sf = self.get_single_file_processor()

        if nc.processamento_em_execucao:
            return nc
        elif tc.processamento_em_execucao:
            return tc
        elif sf.processamento_em_execucao:
            return sf
        try:
            dp = DadosProcessamento.carregar_de_arquivo_json(self.rec.nome_padrao_arquivo_dados_processamento)
            if dp.abrangencia == "web":
                return nc
            elif dp.abrangencia == "twitter":
                return tc
            return sf
        except Exception as e:
            # criar por padrão um news crawler
            return nc

    def get_single_file_processor(self):
        return SingleFileProcessor(self.rec.nome_diretorio_usuario, self.nome_usuario, self.rec)

    def get_news_crawler_facade(self):
        return NewsCrawlerFacade(self.rec.nome_diretorio_usuario, self.nome_usuario, self.rec)

    def get_twitter_crawler_facade(self):
        return TwitterCrawlerFacade(self.rec.nome_diretorio_usuario, self.nome_usuario, self.rec)

    @staticmethod
    def get(user_email):
        return CrawlerFactory(user_email, Recursos(user_email))
