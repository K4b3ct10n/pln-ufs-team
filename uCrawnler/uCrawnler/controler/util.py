# -*- coding: utf-8 -*-

from unicodedata import normalize

import sys
from os.path import join, basename, sep, dirname, exists

from os import listdir, mkdir, remove

from shutil import rmtree, copyfile
from glob import glob
import json
import string
import tarfile
import subprocess
from inspect import getfile
from django.core.files import temp as tempfile
from django.core.serializers.json import DjangoJSONEncoder


import cchardet as chardet
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize



def get_encoding_type(nome_arquivo):
    with open(nome_arquivo,"rb") as arq:
        result = chardet.detect(arq.read())
    return result['encoding']

stop_words = set()


def carregar_stop_words():
    global stop_words
    stop_words = set(stopwords.words('portuguese'))

pontuacao = set()


def carregar_pontuacao():
    global pontuacao
    pontuacao = set(string.punctuation)


def processe_string(args, frase, codif):
    if args.pontuacao and args.stopwords:
        tokens = [token.encode(codif) for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao and token.lower() not in stop_words]
    elif args.pontuacao:
        tokens = [token.encode(codif) for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao]
    elif args.stopwords:
        tokens = [token.encode(codif) for token in word_tokenize(frase, language="portuguese")
                  if token.lower() not in stop_words]
    else:
        tokens = [token.encode(codif) for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao]
        return len(tokens), frase.replace("\n","")

    return len(tokens), " ".join(tokens).decode(codif)


def processe_unicode(args, frase):
    if args.pontuacao and args.stopwords:
        tokens = [token for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao and token.lower() not in stop_words]
    elif args.pontuacao:
        tokens = [token for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao]
    elif args.stopwords:
        tokens = [token for token in word_tokenize(frase, language="portuguese")
                  if token.lower() not in stop_words]
    else:
        tokens = [token for token in word_tokenize(frase, language="portuguese")
                  if token not in pontuacao]
        return len(tokens), frase.replace("\n","")

    return len(tokens), " ".join(tokens)


def compactar_corpora(rec):
    with tarfile.open(rec.nome_completo_arquivo_compactado, "w:gz") as tar:
        tar.add(rec.nome_diretorio_padrao_destino_corpora,
                arcname=basename(rec.nome_diretorio_padrao_destino_corpora))

def salvar_perplexidade(ppl, rec):
    with open(rec.nome_completo_arquivo_perplexidade_saida, "w") as f:
        f.write(ppl)

def copiar_arquivos_url(rec):
    path = rec.nome_diretorio_data_target_usuario
    paths = [join(path, p, "ok") for p in listdir(path)]
    f = rmtree if exists(rec.nome_diretorio_padrao_arquivos_urls) else mkdir
    f(rec.nome_diretorio_padrao_arquivos_urls)
    for p in paths:
        for p in glob(join(p,"*")):
            copyfile(p, join(rec.nome_diretorio_padrao_arquivos_urls, basename(p)))


def executar_operacoes_pos_processamento(rec, args):
    if args.ativar_ngram:
        from language_model_control import LanguageModelController
        lmc = LanguageModelController()
        if args.calcular_perplexidade:
            ppl = lmc.calcular_perplexidade(rec.nome_padrao_corpora, args.numero_gram, rec)
            if ppl:
                salvar_perplexidade(ppl, rec)
            else:
                sys.stderr.write("Erro ao calcular a perplexidade.")
        else:
            lmc.gerar_lm(rec.nome_padrao_corpora, args.numero_gram)

        if args.gerar_vocabulario:
            lmc.gerar_vocabulario(rec.nome_padrao_corpora)

    if args.ativar_entidades:
        from named_entity_control import NamedEntityController
        ner = NamedEntityController()
        ner.configuar_ner(rec.nome_padrao_corpora, args.formato_saida_entidades)

    if hasattr(args, "tipo_saida") and args.tipo_saida == "um_arquivo_por_urlum_arquivo_por_url":
        copiar_arquivos_url(rec)
        remove(rec.nome_padrao_corpora)

    compactar_corpora(rec)


def get_numero_formatado(numero):
    return numero if len(numero) <= 3 else get_numero_formatado(numero[:-3]) + "." + numero[-3:]


def salvar_arquivo_json(nome_arquivo, diconario):
    with open(nome_arquivo, "w") as arquivo:
        json.dump(diconario, arquivo, cls=DjangoJSONEncoder, indent=4, separators=(',', ': '))


def ler_arquivo_json(nome_arquivo):
    with open(nome_arquivo) as arquivo:
        return json.load(arquivo)


def get_tempo_execucao_formatado(tempo_em_segundos):
    tempo_em_segundos = int (tempo_em_segundos)
    horas = tempo_em_segundos/3600
    minutos = (tempo_em_segundos - horas * 3600)/60
    segundos = (tempo_em_segundos - horas * 3600 - minutos * 60)
    tempo = []

    if (horas > 0):
        tempo.append("%dh"%horas)
    if (minutos > 0):
        tempo.append("%dmin"%minutos)
    if (segundos > 0):
        tempo.append("%ds"%segundos)
    return ":".join(tempo)


def obter_diretorio_recurso(nome_arquivo, diretorios=[]):
    dir_ = join(sep, *(dirname(getfile(nome_arquivo)).split(sep)[:-2] + diretorios))
    if "linux" in sys.platform:
        return join(sep, dir_)
    return dir_


def remover_acentos(txt, codif='utf-8'):
    return normalize('NFKD', txt.decode(codif)).encode('ASCII', 'ignore')


text_characters = "".join(map(chr, range(32, 127)) + list("\n\r\t\b"))
_null_trans = string.maketrans("","")


def istextfile(filename, blocksize= 512):
    return istext(open(filename).read(blocksize))


def istext(s):
    if "\0" in s:
        return 0
    if not s:
        return 1
    t = s.translate(_null_trans, text_characters)
    if float(len(t))/len(s) > 0.30:
        return 0
    return 1


def valide_tamanho_arquivo(tamanho, tamanho_em_bytes_maximo):
    if tamanho > tamanho_em_bytes_maximo:
        raise Exception("Arquivo muito grande.")


def valide_arquivo_valido(arquivo):
    if not istextfile(arquivo):
        raise Exception("O arquivo informado não é arquivo de texto.")


# Uma url por linha
def ler_urls_arquivo(arquivo, username):
    valide_tamanho_arquivo(arquivo.size, 5*1024*1024)
    temp_file = join(tempfile.gettempdir(), username)
    with open(temp_file, "w") as f:
        for chunk in arquivo.chunks():
            f.write(chunk)

    valide_arquivo_valido(temp_file)

    with open(temp_file) as f:
        return [line.replace("\n","").strip() for line in f]



def upload_arquivo_perplexidade(arquivo, recursos):
    executar_comando("mkdir -p '%s'" % (recursos.nome_diretorio_permanente_usuario))
    with open(recursos.nome_completo_arquivo_perplexidade_teste, "w") as f:
        for chunk in arquivo:
            f.write(chunk)


def upload_corpus_usuario(arquivo, recursos):
    executar_comando("mkdir -p '%s'" % (recursos.nome_diretorio_permanente_usuario))
    with open(join(recursos.nome_diretorio_permanente_usuario, basename(recursos.nome_padrao_corpora)), "w") as f:
        for chunk in arquivo:
            f.write(chunk)


def upload_dados_acesso_twitter_arquivo(arquivo, username):
    valide_tamanho_arquivo(arquivo.size, 5*1024*1024)
    temp_file = join(tempfile.gettempdir(), username)

    with open(temp_file, "w") as f:
        for chunk in arquivo.chunks():
            f.write(chunk)
    valide_arquivo_valido(temp_file)

    try:
        dados = {}
        with open(temp_file) as f:
            dados["consumer_key"] = f.readline().split("=", 1)[-1].replace("\n","")
            dados["consumer_secret"] = f.readline().split("=", 1)[-1].replace("\n","")
            dados["access_token"] = f.readline().split("=", 1)[-1].replace("\n","")
            dados["access_token_secret"] = f.readline().split("=", 1)[-1].replace("\n","")
        return dados
    except:
        raise Exception("Formato inválido do arquivo")


def executar_comando(*params):
    p = subprocess.Popen(params, stdout=subprocess.PIPE, shell=True)
    out = p.communicate()
    return out
