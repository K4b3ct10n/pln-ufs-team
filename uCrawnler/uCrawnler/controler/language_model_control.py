# -*- coding: utf-8 -*-

import commands


class LanguageModelController(object):

    def gerar_lm(self, nome_corpus, n_gram):
        comando = "ngram-count -text %s -lm  %s.lm -order %d -write %s.write" % \
                  (nome_corpus, nome_corpus, n_gram, nome_corpus)
        output = commands.getoutput(comando)
        return output

    def gerar_vocabulario(self, nome_corpus):
        command = "ngram-count -text %s -write-vocab %s.vocab" % (nome_corpus, nome_corpus)
        output = commands.getoutput(command)
        return output

    def calcular_perplexidade(self, nome_corpus, n_gram, rec):
        self.gerar_lm(nome_corpus, n_gram)
        comando = "ngram -lm %s.lm -ppl %s" % (nome_corpus,
                                                  rec.nome_completo_arquivo_perplexidade_teste)
        output = commands.getoutput(comando)
        try:
            return output.split(":")[1]
        except Exception as e:
            return None
        return output
