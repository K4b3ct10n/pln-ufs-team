# -*- coding: utf-8 -*-

import re
from pprint import pprint
from util import remover_acentos, obter_diretorio_recurso
from os import listdir
from os.path import join
from inspect import currentframe

from recursos import Recursos

DIRETORIO_DOMINIOS = obter_diretorio_recurso(currentframe(), [ "usr", "dmoz", "urls_por_dominio"])


class Dominios(object):

    def __init__(self, diretorio_dominios=DIRETORIO_DOMINIOS, lingua = "pt_br"):
        self.diretorio_dominios = join(diretorio_dominios, lingua)

    def obter_lista_dominios(self, lingua = "pt_br"):
        arquivos = listdir(self.diretorio_dominios, lingua)
        return [arquivo.replace(".txt", "") for arquivo in arquivos]

    def atualizar_lista_dominios_por_arquivo_dmoz(self, nome_arquivo):
        dmoz = Dmoz(self.diretorio_dominios)
        return dmoz.atualizar_pelo_arquivo(nome_arquivo)

    def obter_urls_dominio(self, nome_dominio):
        with open(join(self.diretorio_dominios, nome_dominio + ".txt")) as arq_dominio:
            return arq_dominio.readlines()


class Dmoz(object):
    __arquivo_categorias = {}
    __tag_abertura_topico_educacao = "\s*<Topic r:id=\"Top/World/Portugues/Referencia/(Educacao)(/.+)*\">"
    __tag_abertura_topico = "\s*<Topic r:id=\"Top/World/Portugues/(\w+)(/.+)*\">"
    __tag_fechamento_topico = "\s*</Topic>"
    __tag_link = "\s*<link[1]? r:resource=\"(.+)\"></link[1]?>"

    def __init__(self, diretorio_destino):
        self.diretorio_destino = diretorio_destino

    @classmethod
    def __adicionar_url_para_arquivo_da_cagetoria(cls, nome_categoria, url, diretorio_destino):
        if not (nome_categoria in cls.__arquivo_categorias):
            cls.__arquivo_categorias[nome_categoria] = open(join(diretorio_destino, nome_categoria + ".txt"), "w")
        cls.__arquivo_categorias[nome_categoria].write(url + "\n")

    @classmethod
    def __equivale_a_qualquer_tag_de_abertura_de_topico(cls, linha):
        for pattern in (cls.__tag_abertura_topico_educacao, cls.__tag_abertura_topico):
            match = re.search(pattern, linha)
            if match:
                return match
        return None

    def atualizar_pelo_arquivo(self, nome_arquivo):
        contagem = {}
        with open(nome_arquivo) as arquivo_dmoz:
            esta_dentro_de_topico = False
            categoria_atual = ""
            for linha in arquivo_dmoz:
                linha = remover_acentos(linha)
                if not esta_dentro_de_topico:
                    equivale_tag_de_topico = Dmoz.__equivale_a_qualquer_tag_de_abertura_de_topico(linha)
                    if equivale_tag_de_topico:
                        esta_dentro_de_topico = True
                        categoria_atual = equivale_tag_de_topico.group(1)
                    continue
                equivale_tag_fechamento_topico = re.search(Dmoz.__tag_fechamento_topico, linha)
                if equivale_tag_fechamento_topico:
                    esta_dentro_de_topico = False
                    categoria_atual = ""
                    continue
                equivale_tag_link = re.search(Dmoz.__tag_link, linha)
                if equivale_tag_link:
                    url = equivale_tag_link.group(1)
                    contagem[categoria_atual] = contagem.get(categoria_atual, 0) + 1
                    Dmoz.__adicionar_url_para_arquivo_da_cagetoria(categoria_atual, url, self.diretorio_destino)
        return contagem


def print_info(count):
    exp = {'Artes': 986, 'Ciencia': 459, 'Compras': 104, 'Desportos': 114,
           'Educacao': 343, 'Informatica': 548, 'Jogos': 101, 'Lar': 65, 'Negocios': 949,
           'Noticias': 53, 'Passatempos': 270, 'Referencia': 402, 'Regional': 8100, 'Saude': 273, 'Sociedade': 1015}
    for cat, qtd in count.items():
        print ("%15s: %d/%d (%2.2f%%)" % (cat, qtd, exp[cat], (float(qtd) / exp[cat]) * 100))

if __name__ == "__main__":
    dominios = Dominios(join(Recursos.nome_diretorio_dmoz(), "urls_por_dominio"))
    print_info(dominios.atualizar_lista_dominios_por_arquivo_dmoz("/home/thiago/Downloads/content.rdf.u8"))
    pprint(dominios.obter_lista_dominios())
    pprint(dominios.obter_urls_dominio("Artes"))
