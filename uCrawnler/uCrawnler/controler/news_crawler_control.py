# -*- coding: utf-8 -*-

"""
Created on 30/06/2015

@author: thiago
"""

import os
import subprocess

from random import randint

import sys
from django.utils import timezone
from DadosProcessamento import DadosProcessamento
from dominios import Dominios
from util import executar_comando
from crawler_control_base import CrawlerFacadeBase

class NewsCrawlerFacade(CrawlerFacadeBase):

    def __init__(self, diretorio_trabalho, nome_usuario, recursos):
        super(NewsCrawlerFacade, self).__init__(diretorio_trabalho, nome_usuario, recursos)
        self.crawler = NewsCrawlerControl(diretorio_trabalho, nome_usuario)
        self.dominios = Dominios()
        self.tempo_espera = 5

    # Apaga conteúdo do diretório de trabalho e copia arquivos do crawler
    @property
    def _comando_configurar_diretorio_trabalho(self):
        return "%s/script/configurarDiretorioTrabalhoNewsCrawler.sh '%s' '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.recursos.nome_diretorio_crawler,
                       self.diretorio_trabalho)

    @property
    def status_processamento(self):
        monitor_em_execucao = self.processamento_em_execucao
        if monitor_em_execucao and self.crawler.processamento_em_execucao():
            return "Downloading corpus"
        elif monitor_em_execucao:
            return "Processing corpus"
        elif self.corpus_compactado_gerado:
            return "Done"
        return "Stoped"

    @property
    def contagem_paginas(self):
        return self._ler__arquivo_contagem(self.recursos.nome_padrao_arquivo_contagem_paginas)

    def avaliar_dados_processamento(self, args):
        self.dp = DadosProcessamento.from_dict(args)
        self.dp.arquivo = self.recursos.nome_padrao_arquivo_dados_processamento

    def iniciar_web_crawler_com_urls_sementes(self, **kargs):
        urls_usuario, dominios = kargs["urls_usuario"], kargs["dominios"]
        self.avaliar_dados_processamento(kargs)
        CrawlerFacadeBase.configurar_diretorio_trabalho(self)

        urls_dominios = []
        for d in dominios:
            urls_dominios.extend(self.dominios.obter_urls_dominio(d))

        urls_usuario.extend(urls_dominios)

        self.dp.data_comeco = timezone.now()
        self.crawler.iniciar_web_crawler(urls_sementes=urls_usuario)
        self.dp.salvar()
        self.iniciar_monitor_web_craweler(**self.dp.as_dict())

    def iniciar_web_crawler_com_palavras_chave(self, **kargs):
        self.avaliar_dados_processamento(kargs)
        CrawlerFacadeBase.configurar_diretorio_trabalho(self)
        self.dp.data_comeco = timezone.now()
        self.crawler.iniciar_web_crawler(palavras_chave=kargs["palavras_chave"])
        self.dp.salvar()
        self.iniciar_monitor_web_craweler(**self.dp.as_dict())

    def iniciar_monitor_web_craweler(self, **kargs):
        comando = ["%s/monitor_crawler.py"%os.path.dirname(__file__)]
        comando.extend(["-usuario",  self.nome_usuario])
        comando.extend(["-wait", str(self.tempo_espera)])
        comando.extend(["-cmd_stop", self.crawler.obter_comando_parar_web_crawler()])
        comando.extend(["-cmd_running", self.crawler._comando_processamento_em_execucao()])

        if kargs["ativar_entidades"]:
            comando.append("--ativar_entidades")
            comando.extend(["-formato_saida_entidades", kargs["formato_saida_entidades"]])

        if kargs["ativar_ngram"]:
            if kargs["gerar_vocabulario"]:
                comando.append("--gerar_vocabulario")

            if kargs["calcular_perplexidade"]:
                comando.append("--calcular_perplexidade")

            comando.append("--ativar_ngram")
            comando.extend(["-numero_gram", str(kargs["numero_gram"])])

        if kargs["tipo_saida"]:
            comando.extend(["-tipo_saida", str(kargs["tipo_saida"])])

        if kargs["qtd_palavras"]:
            comando.extend(["-qtd_palavras", str(kargs["qtd_palavras"])])

        if kargs["eliminar_stopwords"]:
            comando.append("--stopwords")

        if kargs["eliminar_pontuacao"]:
            comando.append("--pontuacao")

        if kargs["limpar_caracteres"]:
            comando.append("--limpar_caracteres")

        return subprocess.Popen(comando, stderr=sys.stderr, stdout=sys.stdout).pid

    def parar_crawler(self):
        self.crawler.parar_web_crawler()

    @property
    def _comando_processamento_em_execucao(self):
        return "%s/script/isMonitorWebCrawlerRunning.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.nome_usuario)

class NewsCrawlerControl(object):

    def __init__(self, diretorio_padrao, nome_usuario):
        self.diretorio_raiz = diretorio_padrao
        self.nome_usuario = nome_usuario

    """
        Invoca script que configura o diretario home do nome_usuario como sendo aquele passado
        como parametro (diretorio_trabalho, nesse caso)
    """
    def configurar_diretorio_home(self):
        comando = "%s/script/runConfigHomePath.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)
        return executar_comando(comando)

    """
        Invoca script que limpa os diretorios dentro de saida de dados
    """
    def limpar_diretorios(self):
        comando = "%s/script/runCleanDirs.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)
        return executar_comando(comando)

    """
        Invoca script que se encarrega de inserir as urls sementes no uCrawnler
    """
    def inserir_links_usando_palavras_chave(self, palavras_chave):
        # Verifica se palavras chaves foram informadas, se sim, pesquisa pelas 50 paginas mais relacionadas
        # e configura as urls sementes como sendo tais links
        # Se não for, considera as urls padrão
        urls_sementes = self.run_query_and_return_url_list(palavras_chave)
        self.inserir_links_usando_urls(urls_sementes)

    def inserir_links_usando_urls(self, urls_sementes):
        self.configurar_urls_sementes(urls_sementes)
        comando = "%s/script/runInsertLinks.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)
        return executar_comando(comando)

    """
        Invoca script que se encarrega das urls e em quam ordem eles devem ser enviados ao Crawler
    """
    def executar_link_storage(self):
        comando = "%s/script/runLinkStorage.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)
        return executar_comando(comando)

    """
        Invoca script responsável por iniciar o serviço que armazenará as páginas em disco
    """

    def executar_target_storage(self):
        comando = "%s/script/runTargetStorage.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)
        return executar_comando(comando)

    """
        Invoca script responsável por fazer as pesquisas pelos links ligados as palavras_chave informadas
        e trava seu retorno para criar a lista de links pesquisados
    """

    def run_query_and_return_url_list(self, palavras_chave):
        palavras_chave = [palavra.decode("utf-8") for palavra in palavras_chave]
        comando = "%s/script/runQuery.sh '%s' '%s' " % (self.diretorio_raiz,
                                                        self.diretorio_raiz,
                                                        "+".join(palavras_chave))
        saida = executar_comando(comando)
        saida = saida[0].split("\n")[2:]
        saida = [url for url in saida if url and url.find("Downloading URL") == -1]
        return saida

    """
        Configura em conf/link_storage/news/news.cfg as ursl (SEEDS) passadas em urlsSementes
    """
    def configurar_urls_sementes(self, urls_sementes):
        nome_arquivo = "%s/conf/link_storage/news/news.cfg" % self.diretorio_raiz
        with open(nome_arquivo, "w") as arquivo_news:
            arquivo_news.write(". ./news.template\n")
            arquivo_news.write("SEEDS %s" % (" ".join((url.replace("\n", "") for url in urls_sementes))))

    """
        Configura as urls presentes em conf/link_storage/news/news.default.urls.cfg como sendo as urls de pesquisa
    """
    def configurar_urls_sementes_padrao(self):
        nome_arquivo = "%s/conf/link_storage/news/news.default.urls.cfg" % self.diretorio_raiz
        with open(nome_arquivo) as arquivo_defaul_urls:
            self.configurar_urls_sementes(arquivo_defaul_urls.readlines())

    def processamento_em_execucao(self):
        saida = executar_comando(self._comando_processamento_em_execucao())
        return saida[0] != "\n"

    def _comando_processamento_em_execucao(self):
        return "%s/script/isWebCrawlerRunning.sh '%s'" % (self.diretorio_raiz,  self.nome_usuario)

    """
        Método princial, resposável por iniciar o uCrawnler e todas as suas dependências.
        Exatamente um parâmetro deve ser passado. Ou as urls do usuário ou as suas palavras-chave
    """
    def configurar_portas(self):
        rmi_form_storage_file = "%s/conf/form_storage/rmi_port.cfg" % self.diretorio_raiz
        rmi_link_storage_file = "%s/conf/link_storage/rmi_port.cfg" % self.diretorio_raiz
        rmi_target_storage_file = "%s/conf/target_storage/rmi_port.cfg" % self.diretorio_raiz
        port = tuple(randint(0, 65535) for _ in range(3))

        for i, rmi_file in enumerate((rmi_form_storage_file, rmi_link_storage_file, rmi_target_storage_file)):
            with open(rmi_file,"w") as f:
                f.write("RMI_STORAGE_SERVER_PORT %d" % port[i])

    def iniciar_web_crawler(self, urls_sementes=(), palavras_chave=()):
        self.parar_web_crawler()
        self.configurar_diretorio_home()
        self.limpar_diretorios()
        if urls_sementes:
            self.inserir_links_usando_urls(urls_sementes)
        else:
            self.inserir_links_usando_palavras_chave(palavras_chave),

        self.configurar_portas()

        self.executar_link_storage()
        self.executar_target_storage()

        comando = "%s/script/runWebCrawler.sh '%s'" % (self.diretorio_raiz, self.diretorio_raiz)

        executar_comando(comando)
        return self.processamento_em_execucao()
    """
        Para o uCrawnler invocando o script reponsavel por matar todos os processos relacionados a ele
        (LinkStorage, TargetStorage e CrawlerManager)
    """

    def parar_web_crawler(self):
        return executar_comando(self.obter_comando_parar_web_crawler())

    def obter_comando_parar_web_crawler(self):
        return "%s/script/stopSystem.sh '%s'" % (self.diretorio_raiz,  self.nome_usuario)
