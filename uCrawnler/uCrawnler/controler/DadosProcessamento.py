# -*- coding: utf-8 -*-
__author__ = 'thiago'

import dateutil.parser
from django.utils import timezone
import pytz
from util import ler_arquivo_json, salvar_arquivo_json

class DadosProcessamento():
    def __init__(self, arquivo = None):
        self.arquivo = arquivo
        self.ativar_ngram = False
        self.ativar_entidades = False
        self.gerar_vocabulario = False
        self.numero_gram = 0
        self.limpar_caracteres = False
        self.eliminar_pontuacao = False
        self.eliminar_stopwords = False
        self.calcular_perplexidade = False
        self.formato_saida_entidades = ""
        self.data_comeco = timezone.ZERO
        self.abrangencia = "web"
        self.tipo_saida = "arquivo_unico"
        self.palavras_chave = []
        self.qtd_palavras = 0

    @staticmethod
    def carregar_de_arquivo_json(nome_arquivo_json):
        return DadosProcessamento.from_dict(ler_arquivo_json(nome_arquivo_json))

    def salvar(self, nome_arquivo = None):
        arquivo = nome_arquivo or self.arquivo
        salvar_arquivo_json(arquivo, self.as_dict())

    @staticmethod
    def from_dict(dic):
        dados = DadosProcessamento()
        chaves = set(dados.as_dict().keys())
        for k in set(dic.keys()) & set(chaves):
            if k != "data_comeco":
                setattr(dados, k , dic[k])
            else:
                setattr(dados, k , dateutil.parser.parse(dic[k]).astimezone(pytz.utc))

        # dados.ativar_ngram = bool(dic["ativar_ngram"])
        # dados.ativar_entidades = bool(dic["ativar_entidades"])
        # dados.numero_gram = int(dic["numero_gram"])
        # dados.limpar_caracteres = bool(dic["limpar_caracteres"])
        # dados.eliminiar_pontuacao = bool(dic["eliminar_pontuacao"])
        # dados.eliminar_stopwords = bool(dic["eliminar_stopwords"])
        # dados.qtd_palavras = dic["qtd_palavras"]
        # dados.palavras_chave = dic["palavras_chave"]
        # dados.formato_saida_entidades = dic["formato_saida_entidades"]
        # dados.abrangencia = dic["abrangencia"]
        # dados.calcular_perplexidade = dic["calcular_perplexidade"]
        # if "data_comeco" in dic:
        #        dados.data_comeco = dateutil.parser.parse(dic["data_comeco"]).astimezone(pytz.utc)
        # dados.gerar_vocabulario = bool(dic["gerar_vocabulario"])
        return dados

    def as_dict(self):
        dic = dict(self.__dict__)
        del dic["arquivo"]
        return dic
        # return {"ativar_ngram": self.ativar_ngram,
        #         "ativar_entidades": self.ativar_entidades,
        #         "gerar_vocabulario": self.gerar_vocabulario,
        #         "numero_gram":self.numero_gram,
        #         "limpar_caracteres": self.limpar_caracteres,
        #         "eliminar_pontuacao":self.eliminiar_pontuacao,
        #         "eliminar_stopwords": self.eliminar_stopwords,
        #         "formato_saida_entidades": self.formato_saida_entidades,
        #         "qtd_palavras": self.qtd_palavras,
        #         "palavras_chave": self.palavras_chave,
        #         "abrangencia": self.abrangencia,
        #         "calcular_perplexidade": self.calcular_perplexidade,
        #         "data_comeco": self.data_comeco}
