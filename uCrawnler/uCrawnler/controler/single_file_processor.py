import os
import subprocess

from os.path import basename

import sys

from DadosProcessamento import DadosProcessamento
from util import executar_comando
from crawler_control_base import CrawlerFacadeBase
from django.utils import timezone

class SingleFileProcessor(CrawlerFacadeBase):

    def __init__(self, diretorio_trabalho, nome_usuario, recursos):
        super(SingleFileProcessor, self).__init__(diretorio_trabalho, nome_usuario, recursos)
        self.dp.abrangencia = "corpus_proprio"

    @property
    def _comando_configurar_diretorio_trabalho(self):
        corpus_name = basename(self.recursos.nome_padrao_corpora)
        return "%s/script/configurarDiretorioTrabalhoSingleFileProcessor.sh '%s' '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos,
                       self.recursos.nome_diretorio_usuario,
                       corpus_name)

    @property
    def status_processamento(self):
        monitor_em_execucao = self.processamento_em_execucao
        if monitor_em_execucao:
            return "Processing corpus"
        elif self.corpus_compactado_gerado:
            return "Done"
        return "Stoped"

    @property
    def contagem_palavras(self):
        return self._ler__arquivo_contagem(self.recursos.nome_padrao_arquivo_contagem_palavra)

    def avaliar_dados_processamento(self, args):
        self.dp = DadosProcessamento.from_dict(args)
        self.dp.arquivo = self.recursos.nome_padrao_arquivo_dados_processamento


    def iniciar_single_file_processor(self, kargs):
        self.avaliar_dados_processamento(kargs)
        CrawlerFacadeBase.configurar_diretorio_trabalho(self)

        self.dp.data_comeco = timezone.now()
        self.dp.salvar()
        conf = self.dp.as_dict()
        self.iniciar_processor(conf)

    def iniciar_processor(self, conf):
        comando = ["%s/monitor_single_file_processor.py"%os.path.dirname(__file__)]
        comando.extend(["-usuario",  self.nome_usuario])

        if conf["eliminar_stopwords"]:
            comando.append("--stopwords")

        if conf["eliminar_pontuacao"]:
            comando.append("--pontuacao")

        if conf["limpar_caracteres"]:
            comando.append("--limpar_caracteres")

        if conf["ativar_entidades"]:
            comando.append("--ativar_entidades")
            comando.extend(["-formato_saida_entidades", conf["formato_saida_entidades"]])

        if conf["ativar_ngram"]:
            if conf["gerar_vocabulario"]:
                comando.append("--gerar_vocabulario")

            if conf["calcular_perplexidade"]:
                comando.append("--calcular_perplexidade")

            comando.append("--ativar_ngram")
            comando.extend(["-numero_gram", str(conf["numero_gram"])])

        return subprocess.Popen(comando, stdout= sys.stdout, stderr=sys.stderr).pid

    def parar_crawler(self):
        return executar_comando(self._comando_parar_processor)

    @property
    def _comando_parar_processor(self):
        return "%s/script/stopSingleFileProcessor.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.nome_usuario)

    @property
    def _comando_processamento_em_execucao(self):
        return "%s/script/isSingleFileProcessorRunning.sh '%s'" % \
                      (self.recursos.nome_diretorio_global_recursos, self.nome_usuario)
