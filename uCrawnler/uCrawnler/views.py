# -*- coding: utf-8 -*-
import os
from rexec import FileWrapper
from django.contrib.auth.decorators import login_required
from tweepy.error import TweepError

from controler.crawler_control_base import CrawlerFacadeBase
from forms import WtCoGenForm
from controler.recursos import Recursos
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from controler.util import ler_urls_arquivo, get_tempo_execucao_formatado, \
    get_numero_formatado, upload_dados_acesso_twitter_arquivo, upload_arquivo_perplexidade, \
    upload_corpus_usuario
from controler.crawler_factory import CrawlerFactory


@login_required(login_url='/login/')
def index(request):
    crawler_factory = CrawlerFactory.get(request.user.email)
    if crawler_factory.processamento_em_execucao():
        return redirect('uCrawnler:processamento')

    context = {"corpus_gerado": CrawlerFacadeBase.corpus_compactado_gerado,
               "user": request.user}

    return render(request, "uCrawnler/index.html", context)


def get_dominios_selecionados(dominios):
    if "Todos" in dominios:
        dominios.remove("Todos")
        return dominios
    return dominios


@login_required(login_url='/login/')
def processamento(request):
    crawler_facade = CrawlerFactory.get(request.user.email).get_crawler_em_execucao()

    context = {"status_processamento": crawler_facade.status_processamento,
               "processamento_em_execucao": crawler_facade.processamento_em_execucao,
               "contagem_palavras": get_numero_formatado(str(crawler_facade.contagem_palavras)),
               "tempo_execucao": get_tempo_execucao_formatado(crawler_facade.tempo_processamento),
               "abrangencia": crawler_facade.abrangencia,
               "user": request.user}

    if crawler_facade.abrangencia == "twitter":
        context["contagem_twittes"] = get_numero_formatado(str(crawler_facade.contagem_twittes))
    elif crawler_facade.abrangencia == "web":
        context["contagem_paginas"] = get_numero_formatado(str(crawler_facade.contagem_paginas))
    else:
        context["contagem_paginas"] = 0
    return render(request, "uCrawnler/processamento.html", context)


@login_required(login_url='/login/')
def parar_processamento(request):
    crawler_facade = CrawlerFactory.get(request.user.email).get_crawler_em_execucao()
    crawler_facade.parar_crawler()
    return redirect("uCrawnler:processamento", permanent=True)


@login_required(login_url='/login/')
def download_corpora(request):
    nome_arquivo = Recursos(request.user.email).nome_completo_arquivo_compactado
    wrapper = FileWrapper(file(nome_arquivo))
    response = HttpResponse(wrapper, content_type='application/x-gzip')
    response['Content-Disposition'] = 'attachment; filename= %s' % os.path.basename(nome_arquivo)
    response['Content-Length'] = os.path.getsize(nome_arquivo)
    return response


def processe_se_abrangencia_twitter(request, crawler_factory, abrangencia, arquivo_conf_twitter, opcoes):

    if abrangencia != "twitter":
        return False, None

    crawler_facade = crawler_factory.get_twitter_crawler_facade()
    configuracoes = {}
    erros = {}
    try:
        configuracoes = upload_dados_acesso_twitter_arquivo(arquivo_conf_twitter, request.user.email)
        crawler_facade.validar_credenciais_twitter(configuracoes)
    except TweepError as e:
        erros = {"erros": ["Não foi possível se contectar ao twitter usando as credenciais informadas."]}
    except Exception as e:
        erros = {"erros":(str(e),)}

    if "erros" in erros:
        return True, render(request, "uCrawnler/index.html", erros)

    opcoes.update(configuracoes)
    crawler_facade.iniciar_twitter_crawler(opcoes)

    return True, redirect('uCrawnler:processamento', permanent=True)

def processe_se_abrangencia_web(form, cf, request):
    if form.abrangencia != "web":
        return False, None

    crawler_facade = cf.get_news_crawler_facade()

    dominios = []
    urls_usuario = []

    if form.opcao_coleta_selecionada == "palavras_chave":
        # É preciso que o usuário tenha informado pelo menos uma palavra chave
        if not form.palavras_chave:
            # Exibir mensagem de erro e mandar renderizar
            return render(request, 'uCrawnler/index.html', {"user": request.user})
        crawler_facade.iniciar_web_crawler_com_palavras_chave(**form.opcoes)
        return True, redirect('uCrawnler:processamento', permanent=True)

    if form.opcao_coleta_selecionada == "urls_sementes":
        # É preciso haver, nesse caso, pelo menos um domínio selecionado
        dominios = get_dominios_selecionados(form.slc_dominios)

    elif form.opcao_coleta_selecionada == "arquivo_urls":
        arquivo_urls = form.coleta_arquivo_file
        try:
            urls_usuario = ler_urls_arquivo(arquivo_urls, request.user.email)
        except Exception as e:
            return True, render(request, 'uCrawnler/index.html', {"user": request.user, "erros": (str(e),)})

    crawler_facade.iniciar_web_crawler_com_urls_sementes(urls_usuario= urls_usuario, dominios= dominios, **form.opcoes)

    return True, redirect('uCrawnler:processamento', permanent=True)

def processe_para_abrangencia_arquivo_proprio(form, cf, request):
    upload_corpus_usuario(form.arquivo_corpus_proprio, Recursos(request.user.email))
    sf = cf.get_single_file_processor()
    sf.iniciar_single_file_processor(form.opcoes)
    return redirect('uCrawnler:processamento', permanent=True)



@login_required(login_url='/login/')
def iniciar(request):
    cf = CrawlerFactory.get(request.user.email)
    if cf.processamento_em_execucao():
        return redirect("uCrawnler:processamento", permanent=True)

    if not request.method == "POST":
        return render(request, "uCrawnler/index.html", {"form": WtCoGenForm(), "user": request.user})
    else:
        form = WtCoGenForm(request)

    abrangencia_twitter, retorno_esperado_se_abrangencia_twitter = processe_se_abrangencia_twitter(request, cf, form.abrangencia,
                                                                                                   form.arquivo_conf_twitter,
                                                                                                  form.opcoes)
    if abrangencia_twitter:
        return retorno_esperado_se_abrangencia_twitter

    abrangencia_web, retorno_esperado_se_abrangencia_web = processe_se_abrangencia_web(form, cf, request)

    if form.calcular_perplexidade:
        upload_arquivo_perplexidade(form.arquivo_perplexidade, Recursos(request.user.email))

    if abrangencia_web:
        return retorno_esperado_se_abrangencia_web

    return processe_para_abrangencia_arquivo_proprio(form, cf, request)




