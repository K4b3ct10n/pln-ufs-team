from django import forms


class WtCoGenForm(forms.Form):
    def __init__(self, data = None):
        super(WtCoGenForm, self).__init__(self, data)
        if data:
            self.ativar_ngram = bool(int(data.POST.get(u'checkbox_ativarngram', False)))
            self.ativar_entidades = bool(int(data.POST.get(u'checkbox_ativarentidades', False)))
            self.gerar_vocabulario = bool(int(data.POST.get(u'checkbox_vocabulario', False)))
            self.formato_saida_entidades = data.POST.get(u'formato_saida_entidades')
            self.qtd_palavras = (None if not data.POST.get(u'input_qtd_palavras') else int(data.POST.get(u'input_qtd_palavras')))
            self.numero_gram = int(data.POST.get(u'numero_gram', 0))
            self.abrangencia = data.POST.get(u'abrangencia')
            self.tipo_saida = data.POST.get(u'radio_forma_saida')
            self.palavras_chave = data.POST.get(u'text_palavras_chave', "").encode("utf-8").split(",")
            self.limpar_caracteres = bool(int(data.POST.get("checkbox_limparcaracteres", False)))
            self.eliminar_pontuacao = bool(int(data.POST.get("checkbox_eliminarpontuacao", False)))
            self.eliminar_stopwords = bool(int(data.POST.get("checkbox_eliminarstopwords", False)))
            self.calcular_perplexidade = bool(int(data.POST.get("check_calcular_perplexidade", False)))


            self.arquivo_conf_twitter = data.FILES.get(u'arquivo_dados_acesso_twitter')
            self.coleta_arquivo_file = data.FILES.get(u'coleta_select_file')
            self.arquivo_perplexidade = data.FILES.get(u'file_carregar_arquivo_perplexidade')
            self.arquivo_corpus_proprio = data.FILES.get(u'arquivo_corpus_proprio')
            self.palavras_chave = data.POST.get(u'text_palavras_chave', "").encode("utf-8").split(",")
            self.opcao_coleta_selecionada = data.POST.get(u'fonte_coleta')
            self.slc_dominios = data.POST.getlist(u"slc_dominios[]", [])
            self.create_dict_opcoes()


    def create_dict_opcoes(self):
        self.opcoes = {
            "ativar_ngram": self.ativar_ngram,
            "ativar_entidades": self.ativar_entidades,
            "gerar_vocabulario": self.gerar_vocabulario,
            "formato_saida_entidades": self.formato_saida_entidades,
            "qtd_palavras": self.qtd_palavras,
            "numero_gram": self.numero_gram,
            "abrangencia": self.abrangencia,
            "palavras_chave": self.palavras_chave,
            "limpar_caracteres": self.limpar_caracteres,
            "eliminar_pontuacao": self.eliminar_pontuacao,
            "eliminar_stopwords": self.eliminar_stopwords,
            "calcular_perplexidade": self.calcular_perplexidade,
            "tipo_saida": self.tipo_saida,
        }

    def is_valid(self):
        pass



