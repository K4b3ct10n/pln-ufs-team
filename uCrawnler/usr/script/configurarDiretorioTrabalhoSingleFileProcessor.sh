#!/bin/bash
ls $2 | grep -v keep | xargs rm -rf
mkdir $1/corpus
mkdir $1/keep	
cp $1/keep/$2 $1/corpus
rm $1/keep/$2