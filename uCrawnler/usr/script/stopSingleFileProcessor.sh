#!/bin/sh

kill -9  `ps awux | grep monitor_single_file_processor.py | grep $1 | grep -v perp | grep -v "grep" | awk '{print $2}' | xargs`

sleep 1