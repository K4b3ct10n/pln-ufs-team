#!/bin/bash
cd $2
ls $2 | grep -v keep | xargs rm -rf
mkdir -p $2/corpus
mkdir -p $2/keep
cd $1
cp -n -R * $2
