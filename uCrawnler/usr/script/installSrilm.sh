#!/bin/bash

cd $1
sudo yum -y install gcc
sudo yum -y install gcc-c++
sudo yum -y install zlib-devel.x86_64

mkdir -p srilm

cd srilm
wget http://ftp.gnu.org/gnu/gawk/gawk-4.1.2.tar.gz 
tar -xzf gawk-4.1.2.tar.gz
cd gawk-4.1.2

sudo ./configure

sudo make 
sudo make install

cd $1/srilm

wget https://www.dropbox.com/s/jjsvl62qvs3g3rr/srilm-1.7.1.tar.gz 
mkdir srilm-1.7.1 
tar -xzf srilm-1.7.1.tar.gz -C srilm-1.7.1

cd srilm/srilm-1.7.1 
rm Makefile
wget https://www.dropbox.com/s/cf9252xsa0ips9r/Makefile
make

sudo ln bin/i686-m64/ngram-count /bin/ngram-count

