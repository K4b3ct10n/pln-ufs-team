#!/bin/sh

kill -9  `ps awux | grep CrawlerManager | grep $1 | grep -v perp | grep -v "grep" | awk '{print $2}' | xargs` > /dev/null 2>&1
kill -9  `ps awux | grep Storage | grep -v perp | grep $1 | grep -v "grep" | awk '{print $2}' | xargs` > /dev/null 2>&1

sleep 1