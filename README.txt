Para executar o sistema no LINUX:
	Considerando que o projeto está dentro da pasta pln-ufs-team, entre neste diretório e execute:
		pip install -r requirements.txt
		cd WebCrawler/usr/script/
		.\configNltk.sh
		.\extractSrilm.sh
		mkdir WebCrawler/media

	O programa SRLILM está compilado para para o sistema Debian. Para outros sistemas é preciso recompilar o srilm-1.7.1 e criar um atalho no sistema para o executável ngram-count